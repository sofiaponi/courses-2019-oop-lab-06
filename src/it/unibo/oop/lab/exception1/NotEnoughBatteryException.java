package it.unibo.oop.lab.exception1;

public class NotEnoughBatteryException extends IllegalStateException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int x;
	private int y;
	
	public NotEnoughBatteryException(final int x, final int y) {
		super();
		this.x = x;
		this.y = y;
	}
	
	public String getMessage() {
		return "Not enough battery to move in position ("+this.x+","+this.y+")";
	}

}
