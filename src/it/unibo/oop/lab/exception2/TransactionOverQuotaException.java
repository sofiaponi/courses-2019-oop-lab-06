package it.unibo.oop.lab.exception2;

public class TransactionOverQuotaException extends IllegalStateException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -183608271225525162L;

	public String getMessage() {
		return "The count of ATM transactions gets over the maximum allowed \n" + 
				super.toString();
	}
}
