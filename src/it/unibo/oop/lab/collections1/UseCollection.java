package it.unibo.oop.lab.collections1;

import java.util.*;

/**
 * Example class using {@link java.util.List} and {@link java.util.Map}.
 * 
 */
public final class UseCollection {
	
	private static final int ELEMS = 1_000_000;
    private static final int TO_MS = 1_000_000;

    private UseCollection() {
    }

    /**
     * @param s
     *            unused
     */
    public static void main(final String... s) {
        /*
         * 1) Create a new ArrayList<Integer>, and populate it with the numbers
         * from 1000 (incopenjdk-11-sourceluded) to 2000 (excluded).
         */
    	ArrayList<Integer> array = new ArrayList<>();
    	
    	for( int i = 1; i <= 1000; i++ ) {
    		array.add(i);
    	}
    	
    	System.out.println(array);
    	
        /*
         * 2) Create a new LinkedList<Integer> and, in a single line of code
         * without using any looping construct (for, while), populate it with
         * the same contents of the list of point 1.
         */
    	
    	LinkedList<Integer> list = new LinkedList<>();
    	
    	list.addAll(array);
    	
    	System.out.println(list);
    	
    
        /*
         * 3) Using "set" and "get" and "size" methods, swap the first and last
         * element of the first list. You can not use any "magic number".
         * (Suggestion: use a temporary variable)
         */
    	
    	int first = array.get(0);
    	int last  = array.get(array.size() - 1);
    	
    	array.set(0, last);
    	array.set(array.size() - 1, first);
    	
    	System.out.println(array);
    	
        /*
         * 4) Using a single for-each, print the contents of the arraylist.
         */
    	
    	for(int i : array) {
    		System.out.print(i + ", ");
    	}
    	
    	System.out.println("\n");
    	
        /*
         * 5) Measure the performance of inserting new elements in the head of
         * the collection: measure the time required to add 100.000 elements as
         * first element of the collection for both ArrayList and LinkedList,
         * using the previous lists. In order to measure times, use as example
         * TestPerformance.java.
         */
    	
    	//ARRAYLIST
    	
    	long timearray = System.nanoTime();
    	
    	for(int i = 1; i <= 100_000; i++) {
    		array.add(0, i);
    	}
    	
    	timearray = System.nanoTime() - timearray;
    	
        System.out.println("Add " + ELEMS
                + " in the head of ArrayLIst " + timearray
                + "ns (" + timearray / TO_MS + "ms)");
    	
        //LINKEDLIST
        
        long timelist = System.nanoTime();
    	
    	for(int i = 1; i <= 100_000; i++) {
    		array.add(0, i);
    	}
    	
    	timelist = System.nanoTime() - timelist;
    	
        System.out.println("Add " + ELEMS
                + " in the head of LInkedList " + timelist
                + "ns (" + timelist / TO_MS + "ms)");
        
        
        /*
         * 6) Measure the performance of reading 1000 times an element whose
         * position is in the middle of the collection for both ArrayList and
         * LinkedList, using the collections of point 5. In order to measure
         * times, use as example TestPerformance.java.
         */
        
        //ARRAYLIST
        
        timearray = System.nanoTime();
        
        for(int i = 0; i < 1000; i++) {
        	array.get(array.size()/2);
        }
    	
    	timearray = System.nanoTime() - timearray;
    	
        System.out.println("Read 1000 times the element who is "
        		+"in the middle of the ArrayLIst" + timearray
                + "ns (" + timearray / TO_MS + "ms)");
        
        //LINKEDLIST
        
        timelist = System.nanoTime();
        
        for( int i = 0; i < 1000; i++) {
        	list.get(list.size()/2);
        }
        
        timelist = System.nanoTime() - timelist;
        
        System.out.println("Read 1000 times the element who is "
        		+"in the middle of the LinkedList" + timearray
                + "ns (" + timearray / TO_MS + "ms)");
        
        /*
         * 7) Build a new Map that associates to each continent's name its
         * population:
         * 
         * Africa -> 1,110,635,000
         * 
         * Americas -> 972,005,000
         * 
         * Antarctica -> 0
         * 
         * Asia -> 4,298,723,000
         * 
         * Europe -> 742,452,000
         * 
         * Oceania -> 38,304,000
         */
        
        Map<String, Integer> map = new HashMap<>();      
        map.put("Africa", (int) 111063500L);
        map.put("Americas", (int) 972005000);
        map.put("Antrarctica", (int) 0L);
        map.put("Asia", (int) 4298723000L);
        map.put("Europe", (int) 742452000L);
        map.put("Oceania", (int) 38304000L);
        
        /*
         * 8) Compute the population of the world
         */
        
        long population = 0;
        for(long pop: map.values()) {
        	population += pop;
        }
        
        System.out.println("Population: " + population);
    }
}
